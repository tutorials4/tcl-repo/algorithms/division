#include "div.hpp"

namespace tcl
{
namespace division
{
Engine::Engine() : _result(0) {}
Engine::~Engine() = default;

void Engine::process(int a, int b)
{
    if (b == 0)
        throw;
    _result = a / b;
}

int Engine::result() const
{
    return _result;
}

} // namespace division
} // namespace tcl
