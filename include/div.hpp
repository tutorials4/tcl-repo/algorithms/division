namespace tcl
{
namespace division
{
class Engine
{
public:
    Engine();
    ~Engine();

    void process(int a, int b);

    int result() const;

private:
    int _result;
};
} // namespace division
} // namespace tcl
